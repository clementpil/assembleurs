//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <vector>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND, WTFM};
enum TYPE  {INT, DOUBLE, CHAR, BOOL};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string, TYPE> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
// WhileStatement := "WHILE" Expression DO Statement
// ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
	
		
TYPE Identifier(void){
	
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	TYPE type = DeclaredVariables[lexer->YYText()];
	return type;
}

TYPE Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return INT;
}

TYPE Expression(void);			// Called by Term() and calls Term()

TYPE Factor(void){
	TYPE type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type=Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			Number();
	     	else
				if(current==ID)
					type=Identifier();
				else
					Error("'(' ou chiffre ou lettre attendue");
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	TYPE type1,type2;
	OPMUL mulop;
	type1=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2=Factor();
		if(type2!=type1)
		{
			cout<<"Impossible de combiner "<<type1<<" et "<<type2<<endl; 
			Error("");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	OPADD adop;
	TYPE type1,type2;
	type1=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		type2=Term();
		if(type1!=type2) Error("Les deux facteurs sont de type incompatibles");
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return type1;

}

// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	cout <<"FormatString1:    .string \"%llu\\n\""<<endl;
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables[lexer->YYText()] = INT;
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables[lexer->YYText()] = INT;
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	OPREL oprel;
	TYPE type1,type2;
	type1=SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		type2=SimpleExpression();
		if(type1!=type2) Error("Les deux facteurs sont de type incompatibles");
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOL;
	}
	return type1;
}

// AssignementStatement := Identifier ":=" Expression
string AssignementStatement(void){
	string variable;
	TYPE type1, type2;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type1= DeclaredVariables[variable];
	
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type2=Expression();
	if(type1!=type2) Error("Les deux facteurs sont de type incompatibles");
	cout << "\tpop "<<variable<<endl;
	return variable;
}

void IfStatement();
void WhileStatement();
void ForStatement();
void BlockStatement();
void display();
void CaseStatement();
void DoWhileStatement();


// Statement := AssignementStatement
void Statement(void){
	if(current == ID) AssignementStatement();
	if(current == KEYWORD && strcmp(lexer->YYText(),"IF")==0) IfStatement();
	if(current == KEYWORD && strcmp(lexer->YYText(),"WHILE")==0) WhileStatement();
	if(current == KEYWORD && strcmp(lexer->YYText(),"FOR")==0) ForStatement();
	if(current == KEYWORD && strcmp(lexer->YYText(),"BEGIN")==0) BlockStatement();
	if(current == KEYWORD && strcmp(lexer->YYText(),"DISPLAY")==0) display();

	
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void)
{
	TYPE type;
	int tag = ++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(),"IF")==0)
	{
		current=(TOKEN) lexer->yylex();
		type=Expression();
		if(type!=BOOL)Error("Expression booléenne attendue");	
		cout << "\tpop %rax"<<endl;
		cout << "\tmovq $0, %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		cout << "\tje ELSE"<<tag<<endl;
		
		
		if(current == KEYWORD && strcmp(lexer->YYText(),"THEN")==0)
		{
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\tjmp finif"<<tag<<endl;
			cout << "ELSE"<<tag<<":"<<endl;
		
			if(current == KEYWORD && strcmp(lexer->YYText(),"ELSE")==0)
			{
				current=(TOKEN) lexer->yylex();
				Statement();
			}
		}
		else
		{
			Error("THEN attendu");
		}			
	}
	else
	{
		Error("IF attendu");
	}
	cout<<"finif"<<tag<<":"<<endl;
}

// WhileStatement := "WHILE" Expression DO Statement
void WhileStatement(void)
{
	TYPE type;
	int tag = ++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(),"WHILE")==0)
	{
		current=(TOKEN) lexer->yylex();
		cout << "while"<<tag<<":"<<endl;
		type=Expression();
		if(type!=BOOL)Error("Expression booléenne attendue");	
		cout << "\tpop %rax"<<endl;
		cout << "\tmovq $0, %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		cout << "\tje suite"<<tag<<endl;
		
		if(current == KEYWORD && strcmp(lexer->YYText(),"DO")==0)
		{
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\tjmp while"<<tag<<endl;
			
		}
		else
		{
			Error("DO attendu");
		}
	}
	else 
	{
		Error("WHILE attendu");
	}
	cout<<"suite"<<tag<<":"<<endl;
}

// ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement
void ForStatement(void)
{
	int tag = ++TagNumber;
	if(current==KEYWORD && strcmp(lexer->YYText(), "FOR")==0)//vérifie que le mot clé est bien for
	{
		current=(TOKEN) lexer->yylex();						//prochain TOKEN
		string variable = AssignementStatement();			//AssignementStatement retourne sa variable
		cout << "for"<<tag<<":"<<endl;                    //étiquette pour le retour en fin de DO
		cout << "\tmovq $1, %r8"<<endl;					//initialise step à 1 par défaut
		
        if(current==KEYWORD && strcmp(lexer->YYText(), "TO")==0)
        {
			current=(TOKEN) lexer->yylex();
			Expression();
			
			cout << "\tpop %rbx"<<endl;							//pop le resultat de expression
			cout << "\tmovq $0, %rax"<<endl;	           	 	//place 0 dans rax (car si le resultat de la condition for est faux c'est 0 aussi)
			cout << "\tcmpq %rax, %rbx"<<endl;					//compare rbx et rax (rbx-rax)
			cout << "\tja finfor"<<tag<<endl;					//jump if above à la fin
			
			if(current==KEYWORD && strcmp(lexer->YYText(), "STEP")==0)
			{
				current=(TOKEN) lexer->yylex();
				Number();
				cout << "\tpop %r8"<<endl;						//extrait dans r8 le nombre apres STEP
			}
			
			if(current==KEYWORD && strcmp(lexer->YYText(), "DO")==0)
			{
				current=(TOKEN) lexer->yylex();
				Statement();
				
				cout << "\taddq %r8, "<<variable<<endl;		//le i + step (step = 1 par defaut)
				cout << "\tjmp for"<<tag<<endl;				//retourne au flag début de for
			}
			else{Error("Do attendu");}
		}
		
        else if(current==KEYWORD && strcmp(lexer->YYText(), "DOWNTO")==0)
        {
			current=(TOKEN) lexer->yylex();
			Expression();
			
			cout << "\tpop %rbx"<<endl;						    //pop le resultat de expression
			cout << "\tmovq $0, %rax"<<endl;	                //place 0 dans rax
			cout << "\tcmpq %rax, %rbx"<<endl;				    //compare rbx et rax (rbx-rax)
			cout << "\tja finfor"<<myTag<<endl;				    //jump if below à finfor
			
			if(current==KEYWORD && strcmp(lexer->YYText(), "STEP")==0)
			{
				current=(TOKEN) lexer->yylex();
				Number();
				cout << "\tpop %r8"<<endl;						//extrait dans r8 le nombre apres STEP
			}
			
			if(current==KEYWORD && strcmp(lexer->YYText(), "DO")==0)
			{
				current=(TOKEN) lexer->yylex();
				Statement();
				
				cout << "\tsubq %r8, "<<variable<<endl;			//i - step
				cout << "\tjmp for"<<myTag<<endl;			    //retourne à l'étiquette for (sert à boucler)
			}
			else{Error("DO attendu");}
		}
		else{Error("DOWNTO ou TO attendu");}
	}
	else{Error("FOR attendu");}
	
	cout << "finfor"<<myTag<<":"<<endl;						//étiquette de fin
}

// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void)
{
	int tag = ++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(),"BEGIN")==0)
	{
		current=(TOKEN) lexer->yylex();
		Statement();
		
		if(current == UNKNOWN && strcmp(lexer->YYText(),";")==0)
		{
			current=(TOKEN) lexer->yylex();
			Statement();
		}
		if(current == KEYWORD && strcmp(lexer->YYText(),"END")==0)
		{
			current=(TOKEN) lexer->yylex();
			cout<<"\tjmp suite"<<tag<<endl;			
		}
		else
		{
			Error("END attendu");
		}
	}
	else 
	{
		Error("BEGIN attendu");
	}
	cout<<"suite"<<tag<<":"<<endl;
}

/*--------------------------------TP5----------------------------------*/

void display(void)
{
	int tag = ++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(),"DISPLAY")==0)
	{
		current=(TOKEN) lexer->yylex();
		if(Expression()==INT)
			{				
				cout <<"\tpop %rdx"<<endl;                     // The value to be displayed
				cout <<"\tmovq $FormatString1, %rsi"<<endl;    //"%llu\n"
				cout <<"\tmovl    $1, %edi"<<endl;
				cout <<"\tmovl    $0, %eax"<<endl;
				cout <<"\tcall    __printf_chk@PLT"<<endl;
			}
		else Error("L'expression doit être un INT");
	}
	else Error("DISPLAY attendu");
}
/* à partir d'ici, à cause du confinement je n'ai plus eu de moyen de tester ce que j'ai fait donc il devrait y avoir des erreurs de syntaxes ou autre mais la logique générale ne devrait pas être mauvaise (j'espère)

void CaseStatement(void){
    TYPE type, type2;
    unsigned long tagNum=++TagNumber;
    unsigned long tagNum2=++TagNumber;
    unsigned long tagNumSuiv=TagNumber;
    
    if (strcmp(lexer->YYText(), "CASE") !=0)Error("CASE attendu");
    
    cout << "SWITCHCASE"<< tagNum << " :" << endl;
    current=(TOKEN) lexer->yylex();
    type = SimpleExpression();							
    
    if (type != INT) Error("Entier (int) attendu");
    if (strcmp(lexer->YYText(), "OF") !=0) Error("OF attendu");
    
    cout << "OF"<< tagNum << " :" << endl;
    current=(TOKEN) lexer->yylex();
    cout << "\tpop %rcx" << endl;
    
    while (strcmp(lexer->YYText(), "ELSE") !=0)
    {
        cout << "CASE"<< tagNum2 << " :" << endl;
        cout << "\tmovq %rcx, %rax" << endl;
        type2 = SimpleExpression();
        
        if (type2 != type) Error("Vérifiez les types");
        if (strcmp(lexer->YYText(), ":") !=0) Error(": attendu");
        tagNumSuiv=++TagNumber;
        tagNum2=tagNumSuiv;
        cout << "\tpop %rbx" << endl;					//met tagNum2 dans %rbx
        cout << "\tcmpq %rax, %rbx" << endl;			
        cout << "\tjne CASE" << tagNumSuiv << endl;		//si différents saute au cas tagNumSuiv
        current=(TOKEN) lexer->yylex();
        Statement();
        cout << "\tjmp FINCASE" << tagNum << endl;		
    }
    
    cout << "CASE"<< tagNum2 << " :" << endl;
    current=(TOKEN) lexer->yylex();
    Statement();
    cout << "FINCASE"<< tagNum << " :" << endl;
}

void DoWhileStatement(void){
    TYPE type;
    tag=++TagNumber;
    if (current==KEYWORD && strcmp(lexer->YYText(), "DO")!=0) Error("DO attendu")
    current=(TOKEN) lexer->yylex();
    cout << "\tjmp WHILE" << tag << endl;
    cout << "DO" << tag << " :" << endl;
    Statement();
    if (strcmp(lexer->YYText(), "WHILE") !=0) Error("WHILE attendu");
    current=(TOKEN) lexer->yylex();
    cout << "WHILE" << tag << " :" << endl;
    type = Expression();
    if (type != BOOL) Error("BOOL attendu");
    cout <<"\tpop %rax" << endl;
    cout <<"\tcmpq $0, %rax" << endl;
    cout <<"\tjne DO" <<tag << endl;
    cout <<"FINDOWHILE" <<tag << " :" << endl;
}
*/

//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void){
	int myTag = ++TagNumber;
	if(current==KEYWORD && strcmp(lexer->YYText(), "BEGIN")==0)
	{
		current=(TOKEN) lexer->yylex();	
		Statement();
		
		if(strcmp(lexer->YYText(), ";")==0)
		{
			current=(TOKEN) lexer->yylex();	
			Statement();
		}
		
		if(current==KEYWORD && strcmp(lexer->YYText(), "END")==0)
		{
			current=(TOKEN) lexer->yylex();	
			cout << "\tjmp suite"<<myTag<<endl;
		}
		else{Error("END attendu");}
	}
	else{Error("BEGIN attendu");}
	
	cout << "suite"<<myTag<<":"<<endl;
}



int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





