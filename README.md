TYPES : INT, DOUBLE, CHAR, BOOL

GRAMMAIRE : 

void IfStatement(void)
IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]

void WhileStatement(void)
WhileStatement := "WHILE" Expression DO Statement

void ForStatement(void)
ForStatement := "FOR" AssignementStatement "TO" Expression//"DOWNTO" Expression "STEP" Number "DO" Statement

void BlockStatement(void)
BlockStatement := "BEGIN" Statement { ";" Statement } "END"

void display(void)
DISPLAY <expression>.

void CaseStatement(void)
CASE x OF
            0 : ...
            1 : ...
			...
			ELSE ...

void DoWhileStatement(void)
"DO" Statement
"WHILE" Expression


----------------------------------------------
Rajouts : 
FOR :
	DOWNTO : Pour décrementer
	STEP : Gère les pas (par exemple i+=2 , i+=3...) ATTENTION aux nombres négatif le compilateur ne les gère pas et risque de faire une boucle infinie
DO WHILE : autre méthode pour les boucles
CASE : Pour chaque cas faire telle action

