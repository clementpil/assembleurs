			# This code was produced by the CERI Compiler
FormatString1:    .string "%llu\n"
	.data
	.align 8
b:	.quad 0
z:	.quad 0
a:	.quad 0
c:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $3
	pop b
	push $2
	pop z
	push $0
	pop a
for1:
	push a
	push $3
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jb Vrai2	# If below
	push $0		# False
	jmp Suite2
Vrai2:	push $0xFFFFFFFFFFFFFFFF		# True
Suite2:
	pop %rbx
	movq a , %rax
	cmpq %rax, %rbx
	jb finfor1
	push c
	push z
	pop %rbx
	pop %rax
	mulq	%rbx
	push %rax	# MUL
	pop c
	addq $1 , a
	jmp for1
finfor1:
	push c
	pop %rdx
	movq $FormatString1, %rsi
	movl    $1, %edi
	movl    $0, %eax
	call    __printf_chk@PLT
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
